
//= ../../node_modules/swiper/dist/js/swiper.jquery.js
//= jquery.ddslick.min.js


$(document).ready(function(){
    var mySwiper;

    var settings = function (orientation) {
        return {
            slidesPerView: '2',
            paginationClickable: true,
            spaceBetween: 20,
            direction:orientation,
            loop:true,
            simulateTouch:true,
            observer:true,
            deleteInstance:true,
            breakpoints: {
                767: {
                    slidesPerView: 'auto'
                }
            }
        }
    };

    var startSwiper = function(orientation){
        if(mySwiper) {
            mySwiper.destroy();
            $('.swiper-wrapper, .swiper-slide').attr('style', '');
            $('.swiper1').attr('class', 'swiper1 content-item_block');
        }

            mySwiper = new Swiper('.swiper1', settings(orientation));

    };

    function initSwiper() {
        var screenWidth = $(window).width();

        if(screenWidth > 992) {
            startSwiper('vertical');
        } else {
            startSwiper('horizontal');
        }
    }

    initSwiper();

    $(window).on('resize', function(){
        initSwiper();
    });
});


$('#myDropdown').ddslick({
    onSelected: function(selectedData){
    }
});


$(document).ready(function () {
    var headerHtml = $('.main-header').html();
    $('.visible-tablet').html(headerHtml);
});

// open feedback modal
function openFeedbackModal() {
    $('#feedback').show();
}

$('#btn_feedback').on('click', function (e) {
    e.preventDefault();
    openFeedbackModal();
});

// close modal
function closeModal(){
    $('#conditions').hide();
    $('#feedback').hide();
    $('#succsess_feedback').hide();
}

$('.close_modal').on('click', function () {
    closeModal();
});

// open Succsess Feedback Modal

function openSuccsessFeedbackModal() {
    $('#feedback').hide();
    $('#succsess_feedback').show();
}

$('.btn_confirm').on('click', function (e) {
    e.preventDefault();
    openSuccsessFeedbackModal();
});

// open Terms and Conditions

function openConditionsModal() {
    $('#conditions').show();
}

$("#registration-form-terms-and-conditions-text").on('click', function () {
    openConditionsModal();
});

// open After Registration block
function openAfterRegistration() {
    $('.after-registration').show().css('display','flex');
}

$("#registration-form-terms-button").on('click', function (e) {
    e.preventDefault();
    openAfterRegistration();
});
